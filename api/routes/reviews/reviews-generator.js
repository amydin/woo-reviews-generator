const _ = require('lodash');
const fs = require('fs');
const log = require('debug')('reviews-generator');
const log2 = require('debug')('loop-post-reviews');
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;

module.exports = async (woo, params, csv) => {
    const wooApi = new WooCommerceRestApi({
        url           : woo.url,
        consumerKey   : woo.ck,
        consumerSecret: woo.cs,
        version       : woo.v || "wc/v3",
        timeout       : 3000
    });

    try {
        const jsonString = fs.readFileSync(`${process.env.PWD}/public/random-name.json`);
        const randomName = JSON.parse(jsonString);

        if( !_.isNil(params.products) && !_.isEmpty(params.products) ) {
            params.products = _.replace(params.products, /\s/g, '');
            params.products = _.split(params.products, ',');

            if( _.isNil(params.products) || _.isEmpty(params.products) ) {
                log("format params.products is wrong");
                return null;
            }

            let count = 0;
            for (let index = 0; index < _.size(params.products); index++) {
                const productId   = params.products[index];
                const name        = randomName[_.random(0, _.size(randomName)-1)] || _.uniqueId('Anonymous ');
                const randReview  = unescapeNewline(generateRandReview(csv));
                const emailSuffix = params.email_suffix || "gmail.com";
                const lowRating   = _.toInteger(params.low_rating) || 3;
                const highRating  = _.toInteger(params.high_rating) || 5;

                log2({
                    product_id    : _.toNumber(productId),
                    review        : randReview,
                    reviewer      : name,
                    reviewer_email: `${_.snakeCase(name)}@${emailSuffix}`,
                    rating        : _.random(lowRating, highRating)
                });

                try {
                    await wooApi.post("products/reviews", {
                        product_id    : _.toNumber(productId),
                        review        : randReview,
                        reviewer      : name,
                        reviewer_email: `${_.snakeCase(name)}@${emailSuffix}`,
                        rating        : _.random(lowRating, highRating)
                    });
                    count += 1;
                } catch (err) {
                    log2(err);
                    index -= 1;
                }
            }

            if( _.eq(count, 0) ) {
                log("params.products is null");
                return null;
            }
            return `${count} products have been reviewed`;
        }

        let wooExtQuery = {};
        if( !_.isNil(params.ext_query) && !_.isEmpty(params.ext_query) ) {
            params.ext_query = _.replace(params.ext_query, /\s/g, '');
            params.ext_query = _.split(params.ext_query, '&');

            for (let index = 0; index < params.ext_query.length; index++) {
                let element = params.ext_query[index];
                    element = _.split(element, '=');
                
                wooExtQuery = {
                    ...wooExtQuery,
                    [_.head(element)]: _.last(element)
                };
            }
        }

        let result = null, condLoopPost = true, page = 1, per_page = 5, count = 0;
        while( condLoopPost ) {
            result = await loopPostReviews(wooApi, params, csv, randomName, wooExtQuery, page, per_page, count);

            if( _.isObject(result) ) {
                page     = _.toNumber(result.page);
                per_page = _.toNumber(result.per_page);
                count    = _.toNumber(result.count);
            } else condLoopPost = false;
        }

        return result;
    } catch (err) {
        log(err)
        return null;
    }
}

function loopPostReviews(wooApi, params, csv, randomName, wooExtQuery, page, per_page, count) {
    return (async () => {
        try {
            const products = await wooApi.get("products", {
                page, per_page, ...(wooExtQuery && { ...wooExtQuery })
            });

            if( _.isNil(products) || _.isEmpty(products) || _.isNil(products.data) || _.isEmpty(products.data) ) return `${count} products have been reviewed`;

            for (let index = 0; index < _.size(products.data); index++) {
                const product     = products.data[index];
                const randReview  = unescapeNewline(generateRandReview(csv));
                const name        = randomName[_.random(0, _.size(randomName)-1)] || _.uniqueId('Anonymous ');
                const emailSuffix = params.email_suffix || "gmail.com";
                const lowRating   = _.toInteger(params.low_rating) || 3;
                const highRating  = _.toInteger(params.high_rating) || 5;

                log2({
                    product_id    : _.toNumber(product.id),
                    review        : randReview,
                    reviewer      : name,
                    reviewer_email: `${_.snakeCase(name)}@${emailSuffix}`,
                    rating        : _.random(lowRating, highRating)
                });

                try {
                    await wooApi.post("products/reviews", {
                        product_id    : _.toNumber(product.id),
                        review        : randReview,
                        reviewer      : name,
                        reviewer_email: `${_.snakeCase(name)}@${emailSuffix}`,
                        rating        : _.random(lowRating, highRating)
                    });
                    count += 1;
                } catch(err) {
                    log2(err);
                    index -= 1;
                }
            }

            page += 1; 
            return { page, per_page, count };
        } catch (err) {
            log2(err);
            return null;
        }
    })()
}

function generateRandReview(csv) {
    let generate = true, review;
    while( generate ) {
        review = csv[_.random( 0, _.size( csv )-1 )];
        log(review);
        if( !_.isNil(review) && !_.isEmpty(review) ) generate = false;
    }
    return review;
}

function unescapeNewline(str) {
    return str.toString().replace(/\\n|\\r/g, function (char) {
        switch (char) {
            case "\\n":
                return "\n";
            case "\\r":
                return "\n";
        }
    });
}