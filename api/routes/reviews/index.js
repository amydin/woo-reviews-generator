const express = require("express");

const _ = require('lodash');
const log = require('debug')('generator');

const router = express.Router();

router.get("/", (req, res) => {
    res.redirect('/');
});

router.post("/", async (req, res) => {
    try {
        const woo = req.body.woo;
        if(_.isNil(woo) || _.isEmpty(woo)) {
            log("Woo", woo);
            return res.json({
                status: false,
                data  : null
            })
        }

        const params = req.body.params;
        if(_.isNil(params) || _.isEmpty(params)) {
            log("Params", params);
            return res.json({
                status: false,
                data  : null
            })
        }

        const csv = req.body.csv;
        if(_.isNil(csv) || _.isEmpty(csv) || _.isNil(csv[0]) || _.isEmpty(csv[0])) {
            log("Csv", csv);
            return res.json({
                status: false,
                data  : null
            })
        }

        const result = await require(`./reviews-generator`)(woo, params, csv);

        if(_.isNil(result)) {
            res.json({
                status: false,
                data  : null
            })
        } else {
            res.json({
                status: true,
                data  : result
            })
        }
    } catch (err) {
        log("Catch", err);
        res.json({
            status: false,
            data  : null
        });
    }
});

module.exports = router;
