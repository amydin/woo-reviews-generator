const reviewsRoutes = require("./reviews");

function Router(app, handle) {
  /**
  * get index
  */
  app.get(process.env.API_PATH, (req, res) => {
      res.redirect('/');
  });

  app.use(`${process.env.API_PATH}/reviews`, reviewsRoutes);
}

module.exports = Router;
