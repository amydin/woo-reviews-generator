FROM node:14.1.0
MAINTAINER Amydin S

ENV DEBUG="*,-send*,-follow-redirects*,-morgan*,-compression*,-nodemon*,-snapdragon*,-express*,-body-parser*,-babel*" \ 
    PORT=1717 \ 
    API_PATH="/api"

RUN mkdir -p /app/node_modules && chown -R node:node /app
WORKDIR /app
COPY package*.json ./
USER node
RUN npm install --production
COPY --chown=node:node . .
RUN npm run next

EXPOSE 1717
CMD [ "npm", "start" ]