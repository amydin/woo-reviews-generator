import Head from 'next/head'
import getConfig from 'next/config'
import { useState, useEffect } from 'react'
import { Container, Row, Col, Button, Form, Spinner, ProgressBar } from 'react-bootstrap'
import _ from 'lodash'
import axios from 'axios'
import CSVReader from 'react-csv-reader'
import Cookies from 'universal-cookie'

const cookies = new Cookies();
const { publicRuntimeConfig } = getConfig();
const { API_PATH } = publicRuntimeConfig;

export default function Home() {
  const [isLoading, setLoading] = useState(false)
  const [nowProgress, setNowProgress] = useState(0)
  const [dataCsv, setDataCsv] = useState([])
  const [generatorResult, setGeneratorResult] = useState([])

  const [ipWooUrl, setIpWooUrl] = useState('')
  const [ipWooCk, setIpWooCk] = useState('')
  const [ipWooCs, setIpWooCs] = useState('')
  const [ipWooVs, setIpWooVs] = useState('')
  const [ipSpecProd, setIpSpecProd] = useState('')
  const [ipTotalReview, setTotalReview] = useState('')
  const [ipExtQuery, setIpExtQuery] = useState('')
  const [ipEmailSuffix, setIpEmailSuffix] = useState('')
  const [ipLowRate, setIpLowRate] = useState('')
  const [ipHighRate, setIpHighRate] = useState('')

  useEffect(() => {
    setIpWooUrl(cookies.get('ipWooUrl') ? cookies.get('ipWooUrl') : '');
    setIpWooCk(cookies.get('ipWooCk') ? cookies.get('ipWooCk') : '');
    setIpWooCs(cookies.get('ipWooCs') ? cookies.get('ipWooCs') : '');
    setIpWooVs(cookies.get('ipWooVs') ? cookies.get('ipWooVs') : '');
    setIpSpecProd(cookies.get('ipSpecProd') ? cookies.get('ipSpecProd') : '');
    setTotalReview(cookies.get('ipTotalReview') ? cookies.get('ipTotalReview') : '');
    setIpExtQuery(cookies.get('ipExtQuery') ? cookies.get('ipExtQuery') : '');
    setIpEmailSuffix(cookies.get('ipEmailSuffix') ? cookies.get('ipEmailSuffix') : '');
    setIpLowRate(cookies.get('ipLowRate') ? cookies.get('ipLowRate') : '');
    setIpHighRate(cookies.get('ipHighRate') ? cookies.get('ipHighRate') : '');
  }, [])

  const handleFileLoad = (data, fileInfo) => {
    setDataCsv(data)
    setGeneratorResult([])
    setNowProgress(0)
  }

  const handleSubmit = () => {
    if( _.isNil(ipWooUrl) || _.isEmpty(ipWooUrl) ) return alert("Woo url is required!");
    if( _.isNil(ipWooCk) || _.isEmpty(ipWooCk) ) return alert("Woo consumer key is required!");
    if( _.isNil(ipWooCs) || _.isEmpty(ipWooCs) ) return alert("Woo consumer secret is required!");
    if( _.isNil(ipWooVs) || _.isEmpty(ipWooVs) ) return alert("Woo version is required!");
    if( _.isNil(ipEmailSuffix) || _.isEmpty(ipEmailSuffix) ) return alert("The email suffix is required!");
    if( _.isNil(ipLowRate) || _.isEmpty(ipLowRate) ) return alert("The lowest rating is required!");
    if( _.isNil(ipHighRate) || _.isEmpty(ipHighRate) ) return alert("The highest rating is required!");
    if( _.isNil(dataCsv) || _.isEmpty(dataCsv) ) return alert("Please choose csv file for list of reviews!");
    if( _.isNil(ipTotalReview) || _.isEmpty(ipTotalReview) ) return alert("Total review is required!");

    // Set cookies
    cookies.set('ipWooUrl', ipWooUrl, { path: '/' });
    cookies.set('ipWooCk', ipWooCk, { path: '/' });
    cookies.set('ipWooCs', ipWooCs, { path: '/' });
    cookies.set('ipWooVs', ipWooVs, { path: '/' });
    cookies.set('ipSpecProd', ipSpecProd, { path: '/' });
    cookies.set('ipTotalReview', ipTotalReview, { path: '/' });
    cookies.set('ipExtQuery', ipExtQuery, { path: '/' });
    cookies.set('ipEmailSuffix', ipEmailSuffix, { path: '/' });
    cookies.set('ipLowRate', ipLowRate, { path: '/' });
    cookies.set('ipHighRate', ipHighRate, { path: '/' });
    
    const woo = {
      url: ipWooUrl,
      ck : ipWooCk,
      cs : ipWooCs,
      v  : ipWooVs
    };
    const params = {
      products    : ipSpecProd,
      ext_query   : ipExtQuery,
      email_suffix: ipEmailSuffix,
      low_rating  : ipLowRate,
      high_rating : ipHighRate
    };
    setLoading(true);

    (async function loopGenerateReviews(index, eachProgress, progress, dataCsv, results) {
      let csv = dataCsv[index];

      if(_.gte(index, _.toNumber(ipTotalReview))) csv = null;
      progress = _.floor(_.add(_.toNumber(eachProgress), _.toNumber(progress)), 1);
      index++;

      if(_.gt(progress, 100)) progress = 100;
      try {
        if(_.isNil(csv)) {
          setGeneratorResult(results);
          setDataCsv([]);
          setNowProgress(100);
          return setTimeout(() => setLoading(false), 3000);
        };

        if(_.isEmpty(csv[0])) {
          setNowProgress(progress);
          return loopGenerateReviews(index, eachProgress, progress, dataCsv, results);
        };

        const res = await axios({
          method : 'post',
          url    : `${API_PATH}/reviews`,
          data   : { woo, params, csv },
          timeout: 0
        });

        if(_.isNil(res.data.data) || _.isEmpty(res.data.data)) {
          setNowProgress(progress);
          return loopGenerateReviews(index, eachProgress, progress, dataCsv, results);
        };

        results.push(`${res.data.data}\n`);
        setNowProgress(progress);
        loopGenerateReviews(index, eachProgress, progress, dataCsv, results);
      } catch (err) {
        console.log("req err:", err);
        setNowProgress(progress);
        loopGenerateReviews(index, eachProgress, progress, dataCsv, results);
      }
    })(1, _.divide(100, _.size(dataCsv)), 0, dataCsv, [], [dataCsv[0]])
  }

  const handleLimitRating = (e) => {
    let newVal = 0;
    
    if( _.isNil(e.target.value) || _.isEmpty(e.target.value) ) newVal = '';
    else if( _.lt(e.target.value, 1) ) newVal = 1;
    else if( _.gt(e.target.value, 5) ) newVal = 5;
    else newVal = _.toNumber(e.target.value);

    if( _.eq('lowRating', e.target.name) ) setIpLowRate(newVal);
    else if( _.eq('highRating', e.target.name) ) setIpHighRate(newVal);
    return newVal;
  }

  return (
    <Container className="md-container">
      <Head>
        <title>Generate random reviews for any Products from WooCommerce</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <h1>
          Welcome fans of <a href="https://woocommerce.github.io/woocommerce-rest-api-docs/">Woo</a>!
        </h1>
        <div>
          This site for generate random reviews for any products from <a href="https://woocommerce.github.io/woocommerce-rest-api-docs/">WooCommerce</a>.
        </div>
        <div>The reviewer's name will be generated randomly by our system.</div><br />
        <Container className="md-form">
        <Form>
          <small style={{color: 'red'}}>(*) required</small><br /><br />
          <h5>Woo REST API</h5>
          <small>You can get the keys on <a href="#">https://example.com/wp-admin/admin.php?page=wc-settings&tab=advanced&section=keys</a></small><br /><br />
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              URL*
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="https://example.com" value={ipWooUrl} required onChange={(e) => setIpWooUrl(e.target.value)} />
            </Col>
          </Form.Group>
          
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              Consumer Key*
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="ck_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" value={ipWooCk} required onChange={(e) => setIpWooCk(e.target.value)} />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              Consumer Secret*
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="cs_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" value={ipWooCs} required onChange={(e) => setIpWooCs(e.target.value)} />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              Woo Version*
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="wc/v3" value={ipWooVs} required onChange={(e) => setIpWooVs(e.target.value)} />
            </Col>
          </Form.Group>
          <br />

          <h5>Settings</h5>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              Specific Products <i>(optional)</i>
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="1120, 1230, 1431, ..." value={ipSpecProd} onChange={(e) => setIpSpecProd(e.target.value)} />
              <small><i>You can put specific products ID with comma delimiters</i></small>
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              Extra Parameters <i>(optional)</i>
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="type=variable&status=publish&..." value={ipExtQuery} onChange={(e) => setIpExtQuery(e.target.value)} />
              <small><i>This is extra parameters for GET /products, you can see here <a href="https://woocommerce.github.io/woocommerce-rest-api-docs/#list-all-products" target="_blank">https://woocommerce.github.io/woocommerce-rest-api-docs/#list-all-products</a></i></small>
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
            Email Suffix of Reviewer*
            </Form.Label>
            <Col sm={9}>
              <Form.Control type="text" placeholder="gmail.com" value={ipEmailSuffix} required onChange={(e) => setIpEmailSuffix(e.target.value)} />
              <small><i>The email will be generated from the reviewer's name</i></small>
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
            The Lowest Rating*
            </Form.Label>
            <Col sm={2}>
              <Form.Control type="number" name="lowRating" placeholder="3" min={1} max={5} value={ipLowRate} required onChange={(e) => setIpLowRate(e.target.value)} />
            </Col>
            <Form.Label column sm={3}>
            The Highest Rating*
            </Form.Label>
            <Col sm={2}>
              <Form.Control type="number" name="highRating" placeholder="5" min={1} max={5} value={ipHighRate} required onChange={(e) => setIpHighRate(e.target.value)} />
            </Col>
          </Form.Group>
          <br />
          
          <h5>Reviews</h5>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              CSV File*
            </Form.Label>
            <Col sm={9}>
              <CSVReader 
                onFileLoaded={handleFileLoad} 
                disabled={isLoading}
              />
              <small><i>Download this file for a sample <a href='/sample-reviews.csv'>sample-reviews.csv</a></i></small>
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={3}>
              Total Review*
            </Form.Label>
            <Col sm={2}>
              <Form.Control type="number" placeholder="1" value={ipTotalReview} min={1} onChange={(e) => setTotalReview(e.target.value)} required />
            </Col>
          </Form.Group>

          <Button 
            variant="primary" 
            disabled={isLoading}
            block 
            onClick={() => !isLoading ? handleSubmit() : null}
          >
            {isLoading ? 
            <div>
              <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                aria-hidden="true"
              /> Please wait…
            </div>
            : 'Submit'}
          </Button><br />
          { isLoading && <div><ProgressBar animated now={nowProgress} label={`${nowProgress}%`} /><br /></div> }

          <div align="center">
          { !_.isEmpty(generatorResult) && 
              <pre>
                { generatorResult }
              </pre>
          }
          </div>
        </Form>
        </Container>
      </Container>
      <br />
      <footer className="cntr-footer">
        Powered by&nbsp;
        <a
          href="#"
          target="_blank"
          rel="noopener noreferrer"
        >
          Amydin S
        </a>
      </footer>
    </Container>
  )
}
